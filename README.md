# Does Ruby support method overloading? What is Method Overloading?

Method overloading is a feature that allows a class to have more than one method with same name but different method signature ie. different number of arguments, order of arguments and different types of parameters. Return type is not included in method signature, thus overloaded methods can have different return type.

**Ruby does not support method overloading**

Method overloading is a feature of statically typed language in which binding of methods takes place during compile time. But Ruby being a dynamically typed language, it does not support static binding at all.

The most recent version of the method is considered while ignoring the previously defined versions of the method.

**Example 1**

```ruby
class Test 
    def self.sum(a,b) 
        puts(a+b) 
    end
    def self.sum(a,b,c) 
        puts(a+b+c) 
    end
      
end
Test.sum(1,2)
```
**Output**

```ruby
main.rb:13:in `sum': wrong number of arguments (2 for 3) (ArgumentError)                                                    
        from main.rb:18:in `
```
In Ruby, when a second method is defined with the same name it completely overrides the previously existing method. The previous method is no longer accessible and hence throws error when we try to access it.

**Example 2**
```ruby


brightness_4
class Test 
    def self.sum(a,b) 
        puts(a+b) 
    end
    def self.sum(a,b,c) 
        puts(a+b+c) 
    end
      
end
Test.sum(1,2,7) 
```
**Output**
```ruby
10
```
The second method overwrites the previous method and hence it works absolutely fine when we call the method with three arguments.

<br />

# div and span in HTML?

Div is a block-level element and a block-level element always starts on a new line and takes up the full width available (stretches out to the left and right as far as it can).

The div element is often used as a container for other HTML elements.

When used together with CSS, the div element can be used to style blocks of content.

Span is an inline element and an inline element does not start on a new line and it only takes up as much width as necessary.

The span element is an inline container used to mark up a part of a text, or a part of a document.

When used together with CSS, the span element can be used to style parts of the text.

<br />

# class and a module in Ruby?

Modules are collections of methods and constants. They cannot generate instances. Classes may generate instances (objects), and have per-instance state (instance variables).

Modules may be mixed in to classes and other modules. The mixed in module’s constants and methods blend into that class’s own, augmenting the class’s functionality. Classes, however, cannot be mixed in to anything.

A class may inherit from another class, but not from a module.

A module may not inherit from anything.

<br />

# Restful routes in Rails after scaffold

Rails scaffolding is a quick way to generate some of the major pieces of an application. If you want to create the models, views, and controllers for a new resource in a single operation, scaffolding is the tool for the job.

Scaffold creates **seven** different routes in your application.

Example for a __Photos__ controller:

| HTTP Verb | Path             | Controller#Action | Used for                                     |
|-----------|------------------|-------------------|----------------------------------------------|
| GET       | /photos          | photos#index      | display a list of all photos                 |
| GET       | /photos/new      | photos#new        | return an HTML form for creating a new photo |
| POST      | /photos          | photos#create     | create a new photo                           |
| GET       | /photos/:id      | photos#show       | display a specific photo                     |
| GET       | /photos/:id/edit | photos#edit       | return an HTML form for editing a photo      |
| PATCH/PUT | /photos/:id      | photos#update     | update a specific photo                      |
| DELETE    | /photos/:id      | photos#destroy    | delete a specific photo                      |

<br />

# Hoisting in JS?

In JavaScript, Hoisting is the default behavior of moving all the declarations at the top of the scope before code execution. Basically, it gives us an advantage that no matter where functions and variables are declared, they are moved to the top of their scope regardless of whether their scope is global or local.
It allows us to call functions before even writing them in our code.

Basically, when JavaScript compiles all of your code, all variable declarations using var are lifted to the top of their functional/local scope (if declared inside a function) or to the top of their global scope (if declared outside of a function) regardless of where the actual declaration has been made.

### Variable hoisting
```javascript
console.log(counter); // undefined
var counter = 1;
```
However, the first line of code doesn’t cause an error because the JavaScript engine moves the variable declaration to the top of the script. Technically, the code looks like the following in the execution phase:
```javascript
var counter;

console.log(counter); // undefined
counter = 1;
```

### The `let` keyword
The following declares the variable counter with the let keyword:
```javascript
console.log(counter);
let counter = 1;
```
The JavaScript issues the following error:
```javascript
"ReferenceError: Cannot access 'counter' before initialization
```
The error message explains that the `counter` variable is already in the heap memory. However, it hasn’t initialized.

Behind the scenes, the JavaScript engine hoists the variable declarations that use the let keyword. However, it doesn’t initialize those variables. Notice that if you access a variable that doesn’t exist, the JavaScript will throw a different error:
```javascript
console.log(alien);
let counter = 1;
```
Here is the error:
```javascript
"ReferenceError: alien is not defined
```

### Function hoisting
Functions declarations are also hoisted, but these go to the very top, so will sit above all of the variable declarations.

```javascript
let x = 20,
    y = 10;

let result = add(x,y);
console.log(result);

function add(a, b){
return a + b;
}
```
In this example, we called the add() function before defining it. The above code is equivalent to the following:
```javascript
function add(a, b){
    return a + b;
}

let x = 20,
    y = 10;

let result = add(x,y);
console.log(result);
```
During the creation phase of the execution context, the JavaScript engine places the add() function declaration in the heap memory. To be precise, the JavaScript engine creates an object of the Function type and a function reference called add that refers to the function object.

### Function expressions
The following example changes the `add` from a regular function to a function expression:
```javascript
let x = 20,
    y = 10;

let result = add(x,y);
console.log(result);

var add = function(x, y) {
return x + y;
}
```
If you execute the code, the following error will occur:
```javascript
"TypeError: add is not a function
```
During the creation phase of the global execution context, the JavaScript Engine creates the `add` variable in the memory and initializes its value to `undefined`. When executing the following code, the `add` is `undefined`, hence, it isn’t a function:
```javascript
let result = add(x,y);
```
The `add` variable is assigned to an anonymous function only during the execution phase of the global execution context.

### Arrow functions
The following example changes the add function expression to the _arrow function_:
```javascript
let x = 20,
    y = 10;

let result = add(x,y);
console.log(result);

var add = (x, y) => x + y;
```
The code also issues the same error as the function expression example because arrow functions are syntactic sugar for defining function expressions.
```javascript
"TypeError: add is not a function
```
Similar to the functions expressions, the arrow functions aren’t hoisted.

<br />

# Exception handling in JS

In programming, exception handling is a process or method used for handling the abnormal statements in the code and executing them. It also enables to handle the flow control of the code/program. For handling the code, various handlers are used that process the exception and execute the code. For example, the Division of a non-zero value with zero will result into infinity always, and it is an exception. Thus, with the help of exception handling, it can be executed and handled.

### try/catch/finally
`try/catch/finally` are so called exception handling statements in JavaScript. An exception is an error that occurs at runtime due to an illegal operation during execution. Examples of exceptions include trying to reference an undefined variable, or calling a non existent method.

**Syntax:**
```javascript
try {
    // Code to run
  } catch (e) {
    // Code to run if an exception occurs
  }
  [ // optional
    finally {
      // Code that is always executed regardless of 
      // an exception occurring
    }
  ]
  ```
In the `try` clause, we add code that could potentially generate exceptions. If an exception occurs, the `catch` clause is executed.

Sometimes it is necessary to execute code whether or not it generates an exception. Then we can use the optional block `finally`.

The `finally` block will execute even if the `try` or `catch` clause executes a `return` statement. For example, the following function returns false because the `finally` clause is the last thing to execute.
```javascript
function foo() {
  try {
    return true;
  } finally {
    return false;
  }
}
```
We use `try-catch` in places where we can’t check the correctness of code beforehand.
```javascript
const user = '{"name": "Deepak gupta", "age": 27}';
try {
  // Code to run
  JSON.parse(params)
  // In case of error, the rest of code will never run
  console.log(params)
} catch (err) {
  // Code to run in case of exception
  console.log(err.message)
}
```
As shown above, it’s impossible to check the JSON.parse to have the stringify object or a string before the execution of the code.

<br />

# localStorage and sessionStorage in HTML?

The key difference between localStorage and sessionStorage objects are:

* The localStorage object stores the data without any expiry date. However, **sessionStorage** object stores the data for only one session.
* In the case of a localStorage object, data will not delete when the browser window closes. However, the data get deleted if the browser window closes, in the case of sessionStorage objects.
* The data in sessionStorage is accessible only in the current window of the browser. But, the data in the localStorage can be shared between multiple windows of the browser.

**Example of localStorage**
```javascript
// set key
localStorage.test = 2;

// get key
alert( localStorage.test ); // 2

// remove key
delete localStorage.test;
```
**Example of sessionStorage**
```javascript
// set key
sessionStorage.setItem('test', 1);

// get item
sessionStorage.getItem('test')

// remove item
sessionStorage.removeItem('test')
```

<br />

# Git remote and Git clone?

git remote is used to refer to a remote repository or your central repository.
For e.g: When you want to add a remote repository as your origin, you use this command:
```
git remote add origin <link of your central repository>
```
git clone creates a new git repository by copying an existing one located at the URI you specify.
```
git clone <link_of_your_repository>
```

<br />

# Extend and Include in Ruby?

**Include** is used to importing module code. Ruby will throw an error when we try to access the methods of import module with the class directly because it gets imported as a subclass for the superclass. So, the only way is to access it through the instance of the class.

**Extend** is also used to importing module code but extends import them as class methods. Ruby will throw an error when we try to access methods of import module with the instance of the class because the module gets import to the superclass just as the instance of the extended module. So, the only way is to access it through the class definition.

In simple words, the difference between include and extend is that `include` is for adding methods only to an _instance_ of a class and `extend` is for adding methods to the _class_ but not to its instance.

**Example:**
```ruby
# Creating a module contains a method 
module Geek 
  def geeks 
    puts 'GeeksforGeeks!'
  end
end
   
class Lord 
  
  # only can access geek methods 
  # with the instance of the class. 
  include Geek 
end
   
class Star 
    
  # only can access geek methods 
  # with the class definition. 
  extend Geek 
end
   
# object access  
Lord.new.geeks 
  
# class access 
Star.geeks  
  
# NoMethodError: undefined  method 
# `geeks' for Lord:Class 
Lord.geeks
```
**Output**
```ruby
GeeksforGeeks!
GeeksforGeeks!
main.rb:20:in `': undefined method `geeks' for Lord:Class (NoMethodError)
```
If we want to import instance methods on a class and its class methods too. We can ‘include’ and ‘extend’ it at the same time.
**Example:**
```ruby
# Creating a module contains a method 
module Geek 
  def prints(x) 
    puts x 
  end
end
   
class GFG
  
  # by using both include and exclude 
  # we can access them by both instance 
  #  and class name. 
  include Geek 
  extend Geek 
end
  
# access the prints() in Geek 
# module by include in Lord class 
GFG.new.prints("Howdy") # object access 
   
# access the prints() in Geek  
# module by extend it in Lord class 
GFG.prints("GeeksforGeeks!!") # class access 
```
**Output**
```ruby
Howdy
GeeksforGeeks!!
```

<br />

# Block, Proc and Lamda in Ruby?

Blocks are used for passing blocks of code to methods, and procs and lambda’s allow storing blocks of code in variables.

### Blocks
In Ruby, _blocks_ are snippets of code that can be created to be executed later. Blocks are passed to methods that yield them within the `do` and `end` keywords. One of the many examples is the `#each` method, which loops over enumerable objects.
```ruby
[1,2,3].each do |n|
  puts "#{n}!"
end

[1,2,3].each { |n| puts "#{n}!" } # the one-line equivalent.
```
In this example, a block is passed to the `Array#each` method, which runs the block for each item in the array and prints it to the console.

```ruby
def each
  i = 0
  while i < size
    yield at(i)
    i += 1
  end
end
```
In this simplified example of `Array#each`, in the `while` loop, `yield` is called to execute the passed block for every item in the array. Note that this method has no arguments, as the block is passed to the method implicitly.

### Procs
A “proc” is an instance of the `Proc` class, which holds a code block to be executed, and can be stored in a variable. To create a proc, you call `Proc.new` and pass it a block.
```ruby
proc = Proc.new { |n| puts "#{n}!" }
```
Since a proc can be stored in a variable, it can also be passed to a method just like a normal argument. In that case, we don’t use the ampersand, as the proc is passed explicitly.
```ruby
def run_proc_with_random_number(proc)
  proc.call(random)
end

proc = Proc.new { |n| puts "#{n}!" }
run_proc_with_random_number(proc)
```
Instead of creating a proc and passing that to the method, you can use Ruby’s ampersand parameter syntax that we saw earlier and use a block instead.
```ruby
def run_proc_with_random_number(&proc)
  proc.call(random)
end

run_proc_with_random_number { |n| puts "#{n}!" }
```
Note the added ampersand to the argument in the method. This will convert a passed block to a proc object and store it in a variable in the method scope.

**`#to_proc`**
Symbols, hashes and methods can be converted to procs using their `#to_proc` methods. A frequently seen use of this is passing a proc created from a symbol to a method.
```ruby
[1,2,3].map(&:to_s)
[1,2,3].map {|i| i.to_s }
[1,2,3].map {|i| i.send(:to_s) }
```
This example shows three equivalent ways of calling `#to_s` on each element of the array. In the first one, a symbol, prefixed with an ampersand, is passed, which automatically converts it to a proc by calling its `#to_proc` method. The last two show what that proc could look like.
```ruby
class Symbol
  def to_proc
    Proc.new { |i| i.send(self) }
  end
end
```
Although this is a simplified example, the implementation of `Symbol#to_proc` shows what’s happening under the hood. The method returns a proc which takes one argument and sends `self` to it. Since `self` is the symbol in this context, it calls the `Integer#to_s` method.

### Lambdas
Lambdas are essentially procs with some distinguishing factors. They are more like “regular” methods in two ways: they enforce the number of arguments passed when they’re called and they use “normal” returns.

When calling a lambda that expects an argument without one, or if you pass an argument to a lambda that doesn’t expect it, Ruby raises an `ArgumentError`.
```ruby
irb> lambda (a) { a }.call
ArgumentError: wrong number of arguments (given 0, expected 1)
        from (irb):8:in `block in irb_binding'
        from (irb):8
        from /Users/jeff/.asdf/installs/ruby/2.3.0/bin/irb:11:in `<main>'
```
Also, a lambda treats the return keyword the same way a method does. When calling a proc, the program yields control to the code block in the proc. So, if the proc returns, the current scope returns. If a proc is called inside a function and calls `return`, the function immediately returns as well.
```ruby
def return_from_proc
  a = Proc.new { return 10 }.call
  puts "This will never be printed."
end
```
This function will yield control to the proc, so when it returns, the function returns. Calling the function in this example will never print the output and return 10.
```ruby
def return_from_lambda
  a = lambda { return 10 }.call
  puts "The lambda returned #{a}, and this will be printed."
end
```
When using a lambda, it _will_ be printed. Calling `return` in the lambda will behave like calling `return` in `a` method, so the a variable is populated with `10` and the line is printed to the console.

### Summary (Blocks, procs and lambdas)
* Blocks are used extensively in Ruby for passing bits of code to functions. By using the `yield` keyword, a block can be implicitly passed without having to convert it to a proc.
* When using parameters prefixed with ampersands, passing a block to a method results in a proc in the method’s context. Procs behave like blocks, but they can be stored in a variable.
* Lambdas are procs that behave like methods, meaning they enforce arity and return as methods instead of in their parent scope.

<br />

# References
* https://www.geeksforgeeks.org/method-overloading-in-ruby/
* https://www.w3schools.com/html/html_blocks.asp
* https://www.ruby-lang.org/en/documentation/faq/8/#:~:text=What%20is%20the%20difference%20between%20a%20class%20and%20a%20module,instance%20state%20(instance%20variables).
* https://edgeguides.rubyonrails.org/routing.html#crud-verbs-and-actions
* https://www.javascripttutorial.net/javascript-hoisting/
* https://www.javatpoint.com/exception-handling-in-javascript
* https://blog.logrocket.com/exception-handling-in-javascript/
* https://javascript.info/localstorage
* https://www.edureka.co/community/3573/difference-between-git-remote-and-git-clone
* https://www.geeksforgeeks.org/include-v-s-extend-in-ruby/
* https://blog.appsignal.com/2018/09/04/ruby-magic-closures-in-ruby-blocks-procs-and-lambdas.html
